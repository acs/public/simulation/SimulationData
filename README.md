Contains load, generation and weather profiles.

## Guidelines
- each profile folder should contain one README file which lists references
- normalized profiles should be available for each profile

## Contact
In case of questions contact: Stefan Dähling, Jan Dinkelbach or Markus Mirz