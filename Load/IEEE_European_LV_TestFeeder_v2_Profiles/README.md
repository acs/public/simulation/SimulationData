## General information
Original data were obtained from [Distribution Test Feeders of IEEE PES](https://ewh.ieee.org/soc/pes/dsacom/testfeeders/), webpage accessed in 01/2017

## Description of load profiles
- Original data were provided as [csv](csv)
- [txt](txt) files have been created from the original data and are in a table format, which is expected by Modelica
- Additionally, a standard load profile can be found in [txt](txt)

## Contact
Jan Dinkelbach
