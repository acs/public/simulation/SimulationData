function sum = sumProfiles()    
    cnt = 100;
    
    sum = csvread('Load_profile_1.csv',1,1);

    for i = 2:cnt

        filename = sprintf('Load_profile_%d.csv', i);
    
        disp(filename);

        col = csvread(filename,1,1);
        sum = sum + col;
    end

   sum = sum ./ cnt;
   
   %sum = movmean(sum, 50);
end