import csv
import numpy

# import matplotlib.pyplot as plt

listLoadProfiles = []
for i in range(1, 101):
    with open('LoadProfile_HH' + str(i) + '.txt', 'rb') as sourcefile:
        textreader = csv.reader(sourcefile, delimiter='\t')
        next(textreader)
        next(textreader)
        listLoadProfiles.append([map(float, row) for row in textreader])
arrLoadProfiles = numpy.array(listLoadProfiles)
mean_vals = numpy.mean(arrLoadProfiles, 0)*1000.0
SLP = numpy.stack([numpy.arange(60, 86400 * 2 + 60, 60), numpy.concatenate((mean_vals[:, 1], mean_vals[:, 1]), 0)], 1)
with open('SLP1000_2Days.txt', 'wb') as destfile:
    textwriter = csv.writer(destfile, delimiter='\t')
    textwriter.writerow(['#1'])
    textwriter.writerow(['double LoadProfile(2880,2)'])
    for i in range(numpy.size(SLP, 0)):
        textwriter.writerow(SLP[i])

        # plt.plot(SLP[:,0],SLP[:,1])
        # plt.show()
