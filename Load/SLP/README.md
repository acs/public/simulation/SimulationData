## General information
Contains German standard load profiles (SLP) according to BDEW

## Guidelines on SLPs
* [This file] (dl-Lastprofile-WWE.xls) contains the original profiles for different kind of loads
* For each load type three different seasons are provided: summer (15.5 - 14.9), winter (1.11 - 20.3) and transition time (21.3 - 14.5 and 15.9 - 31.10)
* For each season three different profiles are provided for working days, saturdays and sundays
* Profiles have a granularity of 15min
* The active power consumption is given for each time in kW
* All profiles are normalized to an annual consumption of 1.000 kWh

## Profiles for one year
* For each load type profiles are provided for one year by merging the profiles for different seasons and days
* In [norm_annual_consumption] (norm_annual_consumption) you can find all profiles normalized to an annual consumption of 1.000 kWh
* In [norm_max_power] (norm_max_power) you can find all profiles normalized to the maximum power
* First column: time [s]; Second column: power [W]

## Source 
http://www.westnetz.de/web/cms/de/1772992/westnetz/netz-strom/netznutzung/lastprofile/standard-lastprofile-nach-vdew/

## Contact
sda