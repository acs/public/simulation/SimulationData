load('SLP.mat');



H0 = zeros(35040, 2);
G0 = zeros(35040, 2);
G1 = zeros(35040, 2);
G2 = zeros(35040, 2);
G3 = zeros(35040, 2);
G4 = zeros(35040, 2);
G5 = zeros(35040, 2);
G6 = zeros(35040, 2);
L0 = zeros(35040, 2);
L1 = zeros(35040, 2);
L2 = zeros(35040, 2);

for i = 1:35040
    H0(i,1) = (i-1)*900;
    G0(i,1) = (i-1)*900;
    G1(i,1) = (i-1)*900;
    G2(i,1) = (i-1)*900;
    G3(i,1) = (i-1)*900;
    G4(i,1) = (i-1)*900;
    G5(i,1) = (i-1)*900;
    G6(i,1) = (i-1)*900;
    L0(i,1) = (i-1)*900;
    L1(i,1) = (i-1)*900;
    L2(i,1) = (i-1)*900;
end

for i = 1:79
    if mod(i, 7) < 6 && mod(i, 7) > 0
        for j = 1:96
            H0((i-1)*96 + j, 2) = SLP.H0(j,3) * SLP.H0_dyn(i,1);
            G0((i-1)*96 + j, 2) = SLP.G0(j,3);
            G1((i-1)*96 + j, 2) = SLP.G1(j,3);
            G2((i-1)*96 + j, 2) = SLP.G2(j,3);
            G3((i-1)*96 + j, 2) = SLP.G3(j,3);
            G4((i-1)*96 + j, 2) = SLP.G4(j,3);
            G5((i-1)*96 + j, 2) = SLP.G5(j,3);
            G6((i-1)*96 + j, 2) = SLP.G6(j,3);
            L0((i-1)*96 + j, 2) = SLP.L0(j,3);
            L1((i-1)*96 + j, 2) = SLP.L1(j,3);
            L2((i-1)*96 + j, 2) = SLP.L2(j,3);
        end
    elseif mod(i, 7) == 6
        for j = 1:96
            H0((i-1)*96 + j, 2) = SLP.H0(j,1) * SLP.H0_dyn(i,1);
            G0((i-1)*96 + j, 2) = SLP.G0(j,1);
            G1((i-1)*96 + j, 2) = SLP.G1(j,1);
            G2((i-1)*96 + j, 2) = SLP.G2(j,1);
            G3((i-1)*96 + j, 2) = SLP.G3(j,1);
            G4((i-1)*96 + j, 2) = SLP.G4(j,1);
            G5((i-1)*96 + j, 2) = SLP.G5(j,1);
            G6((i-1)*96 + j, 2) = SLP.G6(j,1);
            L0((i-1)*96 + j, 2) = SLP.L0(j,1);
            L1((i-1)*96 + j, 2) = SLP.L1(j,1);
            L2((i-1)*96 + j, 2) = SLP.L2(j,1);
        end
    else
        for j = 1:96
            H0((i-1)*96 + j, 2) = SLP.H0(j,2) * SLP.H0_dyn(i,1);
            G0((i-1)*96 + j, 2) = SLP.G0(j,2);
            G1((i-1)*96 + j, 2) = SLP.G1(j,2);
            G2((i-1)*96 + j, 2) = SLP.G2(j,2);
            G3((i-1)*96 + j, 2) = SLP.G3(j,2);
            G4((i-1)*96 + j, 2) = SLP.G4(j,2);
            G5((i-1)*96 + j, 2) = SLP.G5(j,2);
            G6((i-1)*96 + j, 2) = SLP.G6(j,2);
            L0((i-1)*96 + j, 2) = SLP.L0(j,2);
            L1((i-1)*96 + j, 2) = SLP.L1(j,2);
            L2((i-1)*96 + j, 2) = SLP.L2(j,2);
        end
    end
end

for i = 80:134
    if mod(i, 7) < 6 && mod(i, 7) > 0
        for j = 1:96
            H0((i-1)*96 + j, 2) = SLP.H0(j,9) * SLP.H0_dyn(i,1);
            G0((i-1)*96 + j, 2) = SLP.G0(j,9);
            G1((i-1)*96 + j, 2) = SLP.G1(j,9);
            G2((i-1)*96 + j, 2) = SLP.G2(j,9);
            G3((i-1)*96 + j, 2) = SLP.G3(j,9);
            G4((i-1)*96 + j, 2) = SLP.G4(j,9);
            G5((i-1)*96 + j, 2) = SLP.G5(j,9);
            G6((i-1)*96 + j, 2) = SLP.G6(j,9);
            L0((i-1)*96 + j, 2) = SLP.L0(j,9);
            L1((i-1)*96 + j, 2) = SLP.L1(j,9);
            L2((i-1)*96 + j, 2) = SLP.L2(j,9);
        end
    elseif mod(i, 7) == 6
        for j = 1:96
            H0((i-1)*96 + j, 2) = SLP.H0(j,7) * SLP.H0_dyn(i,1);
            G0((i-1)*96 + j, 2) = SLP.G0(j,7);
            G1((i-1)*96 + j, 2) = SLP.G1(j,7);
            G2((i-1)*96 + j, 2) = SLP.G2(j,7);
            G3((i-1)*96 + j, 2) = SLP.G3(j,7);
            G4((i-1)*96 + j, 2) = SLP.G4(j,7);
            G5((i-1)*96 + j, 2) = SLP.G5(j,7);
            G6((i-1)*96 + j, 2) = SLP.G6(j,7);
            L0((i-1)*96 + j, 2) = SLP.L0(j,7);
            L1((i-1)*96 + j, 2) = SLP.L1(j,7);
            L2((i-1)*96 + j, 2) = SLP.L2(j,7);
        end
    else
        for j = 1:96
            H0((i-1)*96 + j, 2) = SLP.H0(j,8) * SLP.H0_dyn(i,1);
            G0((i-1)*96 + j, 2) = SLP.G0(j,8);
            G1((i-1)*96 + j, 2) = SLP.G1(j,8);
            G2((i-1)*96 + j, 2) = SLP.G2(j,8);
            G3((i-1)*96 + j, 2) = SLP.G3(j,8);
            G4((i-1)*96 + j, 2) = SLP.G4(j,8);
            G5((i-1)*96 + j, 2) = SLP.G5(j,8);
            G6((i-1)*96 + j, 2) = SLP.G6(j,8);
            L0((i-1)*96 + j, 2) = SLP.L0(j,8);
            L1((i-1)*96 + j, 2) = SLP.L1(j,8);
            L2((i-1)*96 + j, 2) = SLP.L2(j,8);
        end
    end
end

for i = 135:257
    if mod(i, 7) < 6 && mod(i, 7) > 0
        for j = 1:96
            H0((i-1)*96 + j, 2) = SLP.H0(j,6) * SLP.H0_dyn(i,1);
            G0((i-1)*96 + j, 2) = SLP.G0(j,6);
            G1((i-1)*96 + j, 2) = SLP.G1(j,6);
            G2((i-1)*96 + j, 2) = SLP.G2(j,6);
            G3((i-1)*96 + j, 2) = SLP.G3(j,6);
            G4((i-1)*96 + j, 2) = SLP.G4(j,6);
            G5((i-1)*96 + j, 2) = SLP.G5(j,6);
            G6((i-1)*96 + j, 2) = SLP.G6(j,6);
            L0((i-1)*96 + j, 2) = SLP.L0(j,6);
            L1((i-1)*96 + j, 2) = SLP.L1(j,6);
            L2((i-1)*96 + j, 2) = SLP.L2(j,6);
        end
    elseif mod(i, 7) == 6
        for j = 1:96
            H0((i-1)*96 + j, 2) = SLP.H0(j,4) * SLP.H0_dyn(i,1);
            G0((i-1)*96 + j, 2) = SLP.G0(j,4);
            G1((i-1)*96 + j, 2) = SLP.G1(j,4);
            G2((i-1)*96 + j, 2) = SLP.G2(j,4);
            G3((i-1)*96 + j, 2) = SLP.G3(j,4);
            G4((i-1)*96 + j, 2) = SLP.G4(j,4);
            G5((i-1)*96 + j, 2) = SLP.G5(j,4);
            G6((i-1)*96 + j, 2) = SLP.G6(j,4);
            L0((i-1)*96 + j, 2) = SLP.L0(j,4);
            L1((i-1)*96 + j, 2) = SLP.L1(j,4);
            L2((i-1)*96 + j, 2) = SLP.L2(j,4);
        end
    else
        for j = 1:96
            H0((i-1)*96 + j, 2) = SLP.H0(j,5) * SLP.H0_dyn(i,1);
            G0((i-1)*96 + j, 2) = SLP.G0(j,5);
            G1((i-1)*96 + j, 2) = SLP.G1(j,5);
            G2((i-1)*96 + j, 2) = SLP.G2(j,5);
            G3((i-1)*96 + j, 2) = SLP.G3(j,5);
            G4((i-1)*96 + j, 2) = SLP.G4(j,5);
            G5((i-1)*96 + j, 2) = SLP.G4(j,5);
            G6((i-1)*96 + j, 2) = SLP.G6(j,5);
            L0((i-1)*96 + j, 2) = SLP.L0(j,5);
            L1((i-1)*96 + j, 2) = SLP.L1(j,5);
            L2((i-1)*96 + j, 2) = SLP.L2(j,5);
        end
    end
end

for i = 258:304
    if mod(i, 7) < 6 && mod(i, 7) > 0
        for j = 1:96
            H0((i-1)*96 + j, 2) = SLP.H0(j,9) * SLP.H0_dyn(i,1);
            G0((i-1)*96 + j, 2) = SLP.G0(j,9);
            G1((i-1)*96 + j, 2) = SLP.G1(j,9);
            G2((i-1)*96 + j, 2) = SLP.G2(j,9);
            G3((i-1)*96 + j, 2) = SLP.G3(j,9);
            G4((i-1)*96 + j, 2) = SLP.G4(j,9);
            G5((i-1)*96 + j, 2) = SLP.G5(j,9);
            G6((i-1)*96 + j, 2) = SLP.G6(j,9);
            L0((i-1)*96 + j, 2) = SLP.L0(j,9);
            L1((i-1)*96 + j, 2) = SLP.L1(j,9);
            L2((i-1)*96 + j, 2) = SLP.L2(j,9);
        end
    elseif mod(i, 7) == 6
        for j = 1:96
            H0((i-1)*96 + j, 2) = SLP.H0(j,7) * SLP.H0_dyn(i,1);
            G0((i-1)*96 + j, 2) = SLP.G0(j,7);
            G1((i-1)*96 + j, 2) = SLP.G1(j,7);
            G2((i-1)*96 + j, 2) = SLP.G2(j,7);
            G3((i-1)*96 + j, 2) = SLP.G3(j,7);
            G4((i-1)*96 + j, 2) = SLP.G4(j,7);
            G5((i-1)*96 + j, 2) = SLP.G5(j,7);
            G6((i-1)*96 + j, 2) = SLP.G6(j,7);
            L0((i-1)*96 + j, 2) = SLP.L0(j,7);
            L1((i-1)*96 + j, 2) = SLP.L1(j,7);
            L2((i-1)*96 + j, 2) = SLP.L2(j,7);
        end
    else
        for j = 1:96
            H0((i-1)*96 + j, 2) = SLP.H0(j,8) * SLP.H0_dyn(i,1);
            G0((i-1)*96 + j, 2) = SLP.G0(j,8);
            G1((i-1)*96 + j, 2) = SLP.G1(j,8);
            G2((i-1)*96 + j, 2) = SLP.G2(j,8);
            G3((i-1)*96 + j, 2) = SLP.G3(j,8);
            G4((i-1)*96 + j, 2) = SLP.G4(j,8);
            G5((i-1)*96 + j, 2) = SLP.G5(j,8);
            G6((i-1)*96 + j, 2) = SLP.G6(j,8);
            L0((i-1)*96 + j, 2) = SLP.L0(j,8);
            L1((i-1)*96 + j, 2) = SLP.L1(j,8);
            L2((i-1)*96 + j, 2) = SLP.L2(j,8);
        end
    end
end

for i = 305:365
    if mod(i, 7) < 6 && mod(i, 7) > 0
        for j = 1:96
            H0((i-1)*96 + j, 2) = SLP.H0(j,3) * SLP.H0_dyn(i,1);
            G0((i-1)*96 + j, 2) = SLP.G0(j,3);
            G1((i-1)*96 + j, 2) = SLP.G1(j,3);
            G2((i-1)*96 + j, 2) = SLP.G2(j,3);
            G3((i-1)*96 + j, 2) = SLP.G3(j,3);
            G4((i-1)*96 + j, 2) = SLP.G4(j,3);
            G5((i-1)*96 + j, 2) = SLP.G5(j,3);
            G6((i-1)*96 + j, 2) = SLP.G6(j,3);
            L0((i-1)*96 + j, 2) = SLP.L0(j,3);
            L1((i-1)*96 + j, 2) = SLP.L1(j,3);
            L2((i-1)*96 + j, 2) = SLP.L2(j,3);
        end
    elseif mod(i, 7) == 6
        for j = 1:96
            H0((i-1)*96 + j, 2) = SLP.H0(j,1) * SLP.H0_dyn(i,1);
            G0((i-1)*96 + j, 2) = SLP.G0(j,1);
            G1((i-1)*96 + j, 2) = SLP.G1(j,1);
            G2((i-1)*96 + j, 2) = SLP.G2(j,1);
            G3((i-1)*96 + j, 2) = SLP.G3(j,1);
            G4((i-1)*96 + j, 2) = SLP.G4(j,1);
            G5((i-1)*96 + j, 2) = SLP.G5(j,1);
            G6((i-1)*96 + j, 2) = SLP.G6(j,1);
            L0((i-1)*96 + j, 2) = SLP.L0(j,1);
            L1((i-1)*96 + j, 2) = SLP.L1(j,1);
            L2((i-1)*96 + j, 2) = SLP.L2(j,1);
        end
    else
        for j = 1:96
            H0((i-1)*96 + j, 2) = SLP.H0(j,2) * SLP.H0_dyn(i,1);
            G0((i-1)*96 + j, 2) = SLP.G0(j,2);
            G1((i-1)*96 + j, 2) = SLP.G1(j,2);
            G2((i-1)*96 + j, 2) = SLP.G2(j,2);
            G3((i-1)*96 + j, 2) = SLP.G3(j,2);
            G4((i-1)*96 + j, 2) = SLP.G4(j,2);
            G5((i-1)*96 + j, 2) = SLP.G5(j,2);
            G6((i-1)*96 + j, 2) = SLP.G6(j,2);
            L0((i-1)*96 + j, 2) = SLP.L0(j,2);
            L1((i-1)*96 + j, 2) = SLP.L1(j,2);
            L2((i-1)*96 + j, 2) = SLP.L2(j,2);
        end
    end
end

profile = H0;
save('norm_annual_consumption/H0.mat', 'profile', '-v6');
profile = G0;
save('norm_annual_consumption/G0.mat', 'profile', '-v6');
profile = G1;
save('norm_annual_consumption/G1.mat', 'profile', '-v6');
profile = G2;
save('norm_annual_consumption/G2.mat', 'profile', '-v6');
profile = G3;
save('norm_annual_consumption/G3.mat', 'profile', '-v6');
profile = G4;
save('norm_annual_consumption/G4.mat', 'profile', '-v6');
profile = G5;
save('norm_annual_consumption/G5.mat', 'profile', '-v6');
profile = G6;
save('norm_annual_consumption/G6.mat', 'profile', '-v6');
profile = L0;
save('norm_annual_consumption/L0.mat', 'profile', '-v6');
profile = L1;
save('norm_annual_consumption/L1.mat', 'profile', '-v6');
profile = L2;
save('norm_annual_consumption/L2.mat', 'profile', '-v6');


Pmax = max(H0(:,2));
profile = H0;
profile(:,2) = profile(:,2) ./ Pmax;
save('norm_max_power/H0.mat', 'profile', '-v6');
Pmax = max(G0(:,2));
profile = G0;
profile(:,2) = profile(:,2) ./ Pmax;
save('norm_max_power/G0.mat', 'profile', '-v6');
Pmax = max(G1(:,2));
profile = G1;
profile(:,2) = profile(:,2) ./ Pmax;
save('norm_max_power/G1.mat', 'profile', '-v6');
Pmax = max(G2(:,2));
profile = G2;
profile(:,2) = profile(:,2) ./ Pmax;
save('norm_max_power/G2.mat', 'profile', '-v6');
Pmax = max(G3(:,2));
profile = G3;
profile(:,2) = profile(:,2) ./ Pmax;
save('norm_max_power/G3.mat', 'profile', '-v6');
Pmax = max(G4(:,2));
profile = G4;
profile(:,2) = profile(:,2) ./ Pmax;
save('norm_max_power/G4.mat', 'profile', '-v6');
Pmax = max(G5(:,2));
profile = G5;
profile(:,2) = profile(:,2) ./ Pmax;
save('norm_max_power/G5.mat', 'profile', '-v6');
Pmax = max(G6(:,2));
profile = G6;
profile(:,2) = profile(:,2) ./ Pmax;
save('norm_max_power/G6.mat', 'profile', '-v6');
Pmax = max(L0(:,2));
profile = L0;
profile(:,2) = profile(:,2) ./ Pmax;
save('norm_max_power/L0.mat', 'profile', '-v6');
Pmax = max(L1(:,2));
profile = L1;
profile(:,2) = profile(:,2) ./ Pmax;
save('norm_max_power/L1.mat', 'profile', '-v6');
Pmax = max(L2(:,2));
profile = L2;
profile(:,2) = profile(:,2) ./ Pmax;
save('norm_max_power/L2.mat', 'profile', '-v6');
