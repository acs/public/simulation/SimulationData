## General information
The statistical load data are generated with the tool of Kanali Togawa.

## Description of load profiles
- Each .mat file contains 100 stochastical load profiles
- Time resolution of the load profiles is 15 minutes
- The suffix of the .mat file describes the average of the annual energy consumption of each household (unit "kWh"), e.g. "Load_profiles_1000.mat" contains load profiles of households with an annual energy consumption of 1000 kWh

## Additional information
For more information on the statistical load profiles see Togawa's dissertation "Stochastics-based Methods Enabling Testing of Grid-related Algorithms through Simulation".

## Contact
Jan Dinkelbach
