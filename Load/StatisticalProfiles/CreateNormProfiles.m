clear

% Norm profiles to maximum power of each household
load('Load_profiles_4280')
demandProfiles4280_norm=demandProfiles4280./repmat(max(demandProfiles4280),size(demandProfiles4280,1),1);
save('norm_max_power\Load_profiles_4280_norm.mat','demandProfiles4280_norm')