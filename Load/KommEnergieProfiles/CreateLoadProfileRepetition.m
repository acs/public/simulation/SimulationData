clear all
close all
clc

load('KommEnergie_LoadProfile_Summer_H.mat')
Ploadnom=2000;

numDays=1;
LoadProfile=repmat(LoadProfile,numDays,1);
LoadProfile=[LoadProfile;LoadProfile(end,:)];
LoadProfile(:,1)=(0:900:numDays*86400)';

plot(LoadProfile(:,1)/3600,LoadProfile(:,2)*Ploadnom,'LineWidth',3,'Color',[0.8500 0.3250 0.0980])
hold on
plot(LoadProfile(:,1)/3600,LoadProfile(:,2)*Ploadnom,'--','LineWidth',2,'Color',[0 0.4470 0.7410])
xlabel('Time [h]')
ylabel('Load [W]')
xlim([0,24])
set(gca,'XTick',[0:4:numDays*86400/3600])
set(gca,'FontSize',16,'FontWeight','b')
legend('Reality','Forecast')