close all
clear all

load('KommEnergie_LoadProfiles.mat')
TotalLoadProfile=LoadH(:,2);

rel_start_seq=-2;
rel_end_seq=1;
numDays=rel_end_seq-rel_start_seq+1;
startpoint_seq=19872;
Ploadnom=2000;
weeks_ahead=-10;

LoadProfile=TotalLoadProfile(startpoint_seq+rel_start_seq*96:startpoint_seq+rel_end_seq*96+95)*Ploadnom;
plot(LoadProfile,'r')

PredictedLoadProfile=TotalLoadProfile(startpoint_seq+(rel_start_seq-weeks_ahead*7)*96:startpoint_seq+(rel_end_seq-weeks_ahead*7)*96+95)*Ploadnom;
hold on
plot(PredictedLoadProfile,'b')

% Code to find sequence
%load('KommEnergie_LoadProfile_Summer_H.mat')
%LoadProfile=LoadProfile(:,2);

% for i=1:length(TotalLoadProfile)-95
%     if LoadProfile==TotalLoadProfile(i:i+95)
%         idx=i;
%         break;
%     end
% end