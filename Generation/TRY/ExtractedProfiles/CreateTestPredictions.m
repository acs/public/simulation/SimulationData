close all
clear all
clc

addpath('C:\Users\mmi-jdi\Documents\Git_MasterThesisDocs\Code\PVPowerCalculationZhou')
addpath('C:\Users\mmi-jdi\Documents\Git_ModPowerSystems\SimulationData\Load\KommEnergieProfiles')

numDays=6;
Psolarnom=5000;
Ploadnom=2000;
timeInterp=0:60:numDays*86400;

load('TRY7_AvgYear_MaxRad_1Day.mat')
load('KommEnergie_LoadProfile_Summer_H_6Days.mat')
WeatherProfileOrg=WeatherProfile;
LoadProfile(:,2)=LoadProfile(:,2)*Ploadnom;

WeatherProfile=repmat(WeatherProfileOrg,numDays,1);
PredictedWeatherProfile=repmat(WeatherProfileOrg,numDays,1);

% Introduction of impulse change for second day
impulse_height=100;
disturbance=zeros(size(PredictedWeatherProfile,1),2);
disturbance(1.5*length(WeatherProfileOrg),:)=impulse_height;
PredictedWeatherProfile(:,2:3)=PredictedWeatherProfile(:,2:3)-disturbance;

% Add constant to entire curve for third day
const_vals=[0;0;100;0;0;0];
const_mat=repelem(const_vals,length(WeatherProfileOrg),2);
PredictedWeatherProfile(:,2:3)=PredictedWeatherProfile(:,2:3)-const_mat;

% Scaling of entire curve by factor for fourth
scale_facts=[1;1;1;0.5;1;1];
scale_mat=repelem(scale_facts,length(WeatherProfileOrg),2);
PredictedWeatherProfile(:,2:3)=PredictedWeatherProfile(:,2:3).*scale_mat;

% Time shift of 2h for fifth day
tmp=PredictedWeatherProfile(4*24+1:5*24,2:3);
tmp=[tmp(1,:);tmp(1,:);tmp(1:end-2,:)];
PredictedWeatherProfile(4*24+1:5*24,2:3)=tmp;

WeatherProfile=[WeatherProfile;WeatherProfile(end,:)];
WeatherProfile(:,1)=(0:3600:numDays*86400)';

PredictedWeatherProfile=[PredictedWeatherProfile;PredictedWeatherProfile(end,:)];
PredictedWeatherProfile(:,1)=(0:3600:numDays*86400)';

% Plot comparison of radiation profile
figure
plot(WeatherProfile(:,1)/3600,WeatherProfile(:,2)+WeatherProfile(:,3),'LineWidth',3,'Color',[0.8500 0.3250 0.0980])
hold on
plot(PredictedWeatherProfile(:,1)/3600,PredictedWeatherProfile(:,2)+PredictedWeatherProfile(:,3),'--','LineWidth',2,'Color',[0 0.4470 0.7410])
grid on

xlabel('Time [h]')
ylabel('G [W/m�]')
set(gca,'XTick',[0:6:numDays*86400/3600])
xlim([0,(numDays-1)*86400/3600])
ylim([0,1000])
set(gca,'FontSize',16,'FontWeight','b')
legend('Reality','Forecast')

% Calculate resulting solar power profile from radiation data
SolarPowerProfile=zeros(size(WeatherProfile,1),2);
PredictedSolarPowerProfile=zeros(size(WeatherProfile,1),2);

SolarPowerProfile(:,1)=WeatherProfile(:,1);
SolarPowerProfile(:,2)=FuncCalcPVPower(Psolarnom,WeatherProfile(:,2)+WeatherProfile(:,3),WeatherProfile(:,4));
PredictedSolarPowerProfile(:,1)=PredictedWeatherProfile(:,1);
PredictedSolarPowerProfile(:,2)=FuncCalcPVPower(Psolarnom,PredictedWeatherProfile(:,2)+PredictedWeatherProfile(:,3),PredictedWeatherProfile(:,4));

% % Plot comparison of solar power profile
% figure
% plot(SolarPowerProfile(:,1)/3600,SolarPowerProfile(:,2),'LineWidth',3,'Color',[0.8500 0.3250 0.0980])
% hold on
% plot(PredictedSolarPowerProfile(:,1)/3600,PredictedSolarPowerProfile(:,2),'--','LineWidth',2,'Color',[0 0.4470 0.7410])
% grid on
% 
% xlabel('Time [h]')
% ylabel('PV Power [W]')
% set(gca,'XTick',[0:6:numDays*86400/3600])
% xlim([0,(numDays-1)*86400/3600])
% ylim([0,5000])
% set(gca,'FontSize',16,'FontWeight','b')
% legend('Reality','Forecast')

% Interpolate solar power profile and load power profile to have same time
% axis
SolarPowerProfileInterp(:,1)=timeInterp;
SolarPowerProfileInterp(:,2)=interp1(SolarPowerProfile(:,1),SolarPowerProfile(:,2),timeInterp);
PredictedSolarPowerProfileInterp(:,1)=timeInterp;
PredictedSolarPowerProfileInterp(:,2)=interp1(PredictedSolarPowerProfile(:,1),PredictedSolarPowerProfile(:,2),timeInterp);
LoadProfileInterp(:,1)=timeInterp;
LoadProfileInterp(:,2)=interp1(LoadProfile(:,1),LoadProfile(:,2),timeInterp);

% Plot comparison of resulting generation disturbance
figure
plot(SolarPowerProfileInterp(:,1)/3600,SolarPowerProfileInterp(:,2)-LoadProfileInterp(:,2),'LineWidth',3,'Color',[0.8500 0.3250 0.0980])
hold on
plot(PredictedSolarPowerProfileInterp(:,1)/3600,PredictedSolarPowerProfileInterp(:,2)-LoadProfileInterp(:,2),'--','LineWidth',2,'Color',[0 0.4470 0.7410])
grid on

xlabel('Time [h]')
ylabel('Net generation [W]')
set(gca,'XTick',[0:6:numDays*86400/3600])
xlim([0,(numDays-1)*86400/3600])
set(gca,'FontSize',16,'FontWeight','b')
legend('Reality','Forecast')

% % Plot comparison of radiation profile (Ideal forecast)
% figure
% plot(PredictedWeatherProfile(:,1)/3600,PredictedWeatherProfile(:,2)+PredictedWeatherProfile(:,3),'LineWidth',3,'Color',[0.8500 0.3250 0.0980])
% hold on
% plot(PredictedWeatherProfile(:,1)/3600,PredictedWeatherProfile(:,2)+PredictedWeatherProfile(:,3),'--','LineWidth',2,'Color',[0 0.4470 0.7410])
% grid on
% 
% xlabel('Time [h]')
% ylabel('G [W/m�]')
% set(gca,'XTick',[0:6:numDays*86400/3600])
% xlim([0,(numDays-1)*86400/3600])
% ylim([0,1000])
% set(gca,'FontSize',16,'FontWeight','b')
% legend('Reality','Forecast')
