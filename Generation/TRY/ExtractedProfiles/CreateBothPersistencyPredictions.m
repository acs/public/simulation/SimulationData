clear all
close all

ImportTRYData

rel_start_seq=0;
rel_end_seq=0;
numDays=rel_end_seq-rel_start_seq+1;
Psolarnom=5000;
Ploadnom=2000;
timeInterp=0:60:numDays*86400;

load('C:\Users\mmi-jdi\Documents\Git_ModPowerSystems\SimulationData\Load\KommEnergieProfiles\KommEnergie_LoadProfile_Summer_H_5Days.mat')
LoadProfile(:,2)=LoadProfile(:,2)*Ploadnom;

[max_val,max_idx]=max(TRY201007Jahr.B+TRY201007Jahr.D);
bool_month=(table2array(TRY201007Jahr(:,3))==table2array(TRY201007Jahr(max_idx,3)));
bool_day=(table2array(TRY201007Jahr(:,4))>=(table2array(TRY201007Jahr(max_idx,4)))+rel_start_seq)&(table2array(TRY201007Jahr(:,4))<=(table2array(TRY201007Jahr(max_idx,4)))+rel_end_seq+1);
DataSelectedDays=TRY201007Jahr(bool_month&bool_day,:);

WeatherProfile(:,2)=DataSelectedDays.B;
WeatherProfile(:,3)=DataSelectedDays.D;
WeatherProfile(:,4)=DataSelectedDays.t;

WeatherProfile=[WeatherProfile;WeatherProfile(end,:)];
WeatherProfile(:,1)=(0:3600:(rel_end_seq-rel_start_seq+2)*86400)';

bool_month=(table2array(TRY201007Jahr(:,3))==table2array(TRY201007Jahr(max_idx,3)));
bool_day=(table2array(TRY201007Jahr(:,4))>=(table2array(TRY201007Jahr(max_idx,4)))+rel_start_seq-1)&(table2array(TRY201007Jahr(:,4))<=(table2array(TRY201007Jahr(max_idx,4)))+rel_end_seq);
DataSelectedDaysPrediction=TRY201007Jahr(bool_month&bool_day,:);

PredictedWeatherProfile(:,2)=DataSelectedDaysPrediction.B;
PredictedWeatherProfile(:,3)=DataSelectedDaysPrediction.D;
PredictedWeatherProfile(:,4)=DataSelectedDaysPrediction.t;

PredictedWeatherProfile=[PredictedWeatherProfile;PredictedWeatherProfile(end,:)];
PredictedWeatherProfile(:,1)=(0:3600:(rel_end_seq-rel_start_seq+2)*86400)';

% Plot comparison of radiation profile
figure(1)
plot(WeatherProfile(:,1)/3600,WeatherProfile(:,2)+WeatherProfile(:,3),'LineWidth',3,'Color',[0.8500 0.3250 0.0980])
hold on
plot(PredictedWeatherProfile(:,1)/3600,PredictedWeatherProfile(:,2)+PredictedWeatherProfile(:,3),'--','LineWidth',2,'Color',[0 0.4470 0.7410])
grid on

xlabel('Time [h]')
ylabel('G [W/m�]')
set(gca,'XTick',[0:4:numDays*86400/3600])
xlim([0,(rel_end_seq-rel_start_seq+1)*86400/3600])
set(gca,'FontSize',16,'FontWeight','b')
legend('Reality','Forecast')

% Calculate resulting solar power profile from radiation data
SolarPowerProfile=zeros(size(WeatherProfile,1),2);
PredictedSolarPowerProfile=zeros(size(WeatherProfile,1),2);

SolarPowerProfile(:,1)=WeatherProfile(:,1);
SolarPowerProfile(:,2)=FuncCalcPVPower(Psolarnom,WeatherProfile(:,2)+WeatherProfile(:,3),WeatherProfile(:,4));
PredictedSolarPowerProfile(:,1)=PredictedWeatherProfile(:,1);
PredictedSolarPowerProfile(:,2)=FuncCalcPVPower(Psolarnom,PredictedWeatherProfile(:,2)+PredictedWeatherProfile(:,3),PredictedWeatherProfile(:,4));

% Plot comparison of solar power profile
figure(2)
plot(SolarPowerProfile(:,1)/3600,SolarPowerProfile(:,2),'LineWidth',3,'Color',[0.8500 0.3250 0.0980])
hold on
plot(PredictedSolarPowerProfile(:,1)/3600,PredictedSolarPowerProfile(:,2),'--','LineWidth',2,'Color',[0 0.4470 0.7410])
grid on

xlabel('Time [h]')
ylabel('PV Power [W]')
set(gca,'XTick',[0:4:numDays*86400/3600])
xlim([0,numDays*86400/3600])
ylim([0,5000])
set(gca,'FontSize',16,'FontWeight','b')
legend('Reality','Forecast')

% Interpolate solar power profile and load power profile to have same time
% axis
SolarPowerProfileInterp(:,1)=timeInterp;
SolarPowerProfileInterp(:,2)=interp1(SolarPowerProfile(:,1),SolarPowerProfile(:,2),timeInterp);
PredictedSolarPowerProfileInterp(:,1)=timeInterp;
PredictedSolarPowerProfileInterp(:,2)=interp1(PredictedSolarPowerProfile(:,1),PredictedSolarPowerProfile(:,2),timeInterp);
LoadProfileInterp(:,1)=timeInterp;
LoadProfileInterp(:,2)=interp1(LoadProfile(:,1),LoadProfile(:,2),timeInterp);

% Plot comparison of resulting generation disturbance
figure(3)
plot(SolarPowerProfileInterp(:,1)/3600,SolarPowerProfileInterp(:,2)-LoadProfileInterp(:,2),'LineWidth',3,'Color',[0.8500 0.3250 0.0980])
hold on
plot(PredictedSolarPowerProfileInterp(:,1)/3600,PredictedSolarPowerProfileInterp(:,2)-LoadProfileInterp(:,2),'--','LineWidth',2,'Color',[0 0.4470 0.7410])
grid on

xlabel('Time [h]')
ylabel('Disturbance generation [W]')
set(gca,'XTick',[0:4:numDays*86400/3600])
xlim([0,numDays*86400/3600])
set(gca,'FontSize',16,'FontWeight','b')
legend('Reality','Forecast')
