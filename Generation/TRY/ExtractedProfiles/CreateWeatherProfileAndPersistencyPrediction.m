clear all
close all
clc

filename='C:\Workspace\SimulationData\Generation\TRY\ExtractedProfiles\Region13\TRY2010_13_Jahr.csv';

TRYData=ImportTRYData(filename);

rel_start_seq=0;
rel_end_seq=0;
numDays=rel_end_seq-rel_start_seq+1;
Psolarnom=5000;
timeInterp=0:60:numDays*86400;

% Get selected days around max radiation day
[max_val,max_idx]=max(TRYData.B+TRYData.D);
bool_month=(table2array(TRYData(:,3))==table2array(TRYData(max_idx,3)));
bool_day=(table2array(TRYData(:,4))>=(table2array(TRYData(max_idx,4)))+rel_start_seq)&(table2array(TRYData(:,4))<=(table2array(TRYData(max_idx,4)))+rel_end_seq);
DataSelectedDays=TRYData(bool_month&bool_day,:);

% Fill data in weather profile
WeatherProfile(:,2)=DataSelectedDays.B;
WeatherProfile(:,3)=DataSelectedDays.D;
WeatherProfile(:,4)=DataSelectedDays.t;
WeatherProfile(:,1)=(0:3600:numDays*86400-3600)';

% Get corresponding data for persistency forecast
bool_month=(table2array(TRYData(:,3))==table2array(TRYData(max_idx,3)));
bool_day=(table2array(TRYData(:,4))>=(table2array(TRYData(max_idx,4)))+rel_start_seq-1)&(table2array(TRYData(:,4))<=(table2array(TRYData(max_idx,4)))+rel_end_seq-1);
DataSelectedDaysPrediction=TRYData(bool_month&bool_day,:);

% Fill data in weather profile
PredictedWeatherProfile(:,2)=DataSelectedDaysPrediction.B;
PredictedWeatherProfile(:,3)=DataSelectedDaysPrediction.D;
PredictedWeatherProfile(:,4)=DataSelectedDaysPrediction.t;
PredictedWeatherProfile(:,1)=(0:3600:numDays*86400-3600)';

% Plot comparison of radiation profiles
figure
plot(WeatherProfile(:,1)/3600,WeatherProfile(:,2)+WeatherProfile(:,3),'LineWidth',3,'Color',[0.8500 0.3250 0.0980])
hold on
plot(PredictedWeatherProfile(:,1)/3600,PredictedWeatherProfile(:,2)+PredictedWeatherProfile(:,3),'--','LineWidth',2,'Color',[0 0.4470 0.7410])
grid on

xlabel('Time [h]')
ylabel('G [W/m�]')
set(gca,'XTick', 0:4:numDays*86400/3600)
% xlim([0,(rel_end_seq-rel_start_seq+1)*86400/3600])
set(gca,'FontSize',16,'FontWeight','b')
legend('Reality','Forecast')

% % Calculate resulting solar power profile from radiation data
% SolarPowerProfile=zeros(size(WeatherProfile,1),2);
% PredictedSolarPowerProfile=zeros(size(WeatherProfile,1),2);
% 
% SolarPowerProfile(:,1)=WeatherProfile(:,1);
% SolarPowerProfile(:,2)=FuncCalcPVPower(Psolarnom,WeatherProfile(:,2)+WeatherProfile(:,3),WeatherProfile(:,4));
% PredictedSolarPowerProfile(:,1)=PredictedWeatherProfile(:,1);
% PredictedSolarPowerProfile(:,2)=FuncCalcPVPower(Psolarnom,PredictedWeatherProfile(:,2)+PredictedWeatherProfile(:,3),PredictedWeatherProfile(:,4));
% 
% % Plot comparison of solar power profile
% figure(2)
% plot(SolarPowerProfile(:,1)/3600,SolarPowerProfile(:,2),'LineWidth',3,'Color',[0.8500 0.3250 0.0980])
% hold on
% plot(PredictedSolarPowerProfile(:,1)/3600,PredictedSolarPowerProfile(:,2),'--','LineWidth',2,'Color',[0 0.4470 0.7410])
% grid on
% 
% xlabel('Time [h]')
% ylabel('PV Power [W]')
% set(gca,'XTick',[0:4:numDays*86400/3600])
% xlim([0,numDays*86400/3600])
% ylim([0,5000])
% set(gca,'FontSize',16,'FontWeight','b')
% legend('Reality','Forecast')
