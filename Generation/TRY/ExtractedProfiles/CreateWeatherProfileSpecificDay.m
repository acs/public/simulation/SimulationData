clear all
close all
clc

filename='C:\Workspace\SimulationData\Generation\TRY\ExtractedProfiles\Region13\TRY2010_13_Jahr.csv';

TRYData=ImportTRYData(filename);

%% Maximum radiation day
% Get max radiation day
[max_val,max_idx]=max(TRYData.B+TRYData.D);
bool_month=(table2array(TRYData(:,3))==table2array(TRYData(max_idx,3)));
bool_day=(table2array(TRYData(:,4))>=(table2array(TRYData(max_idx,4))))&(table2array(TRYData(:,4))<=(table2array(TRYData(max_idx,4))));
DataSelectedDays=TRYData(bool_month&bool_day,:);

% Fill data in weather profile
WeatherProfileMax(:,2)=DataSelectedDays.B;
WeatherProfileMax(:,3)=DataSelectedDays.D;
WeatherProfileMax(:,4)=DataSelectedDays.t;
WeatherProfileMax(:,1)=(0:3600:86400-3600)';

%% Average radiation day
% Fill average data in weather profile
for i=1:24 
    WeatherProfileAvg(i,2)=mean(table2array(TRYData(i:24:8760,14)));
    WeatherProfileAvg(i,3)=mean(table2array(TRYData(i:24:8760,15)));
    WeatherProfileAvg(i,4)=mean(table2array(TRYData(i:24:8760,9)));
end
WeatherProfileAvg(:,1)=(0:3600:86400-3600)';

figure(1)
plot(WeatherProfileMax(:,1),WeatherProfileMax(:,2)+WeatherProfileMax(:,3),WeatherProfileAvg(:,1),WeatherProfileAvg(:,2)+WeatherProfileAvg(:,3))
figure(2)
plot(WeatherProfileMax(:,1),WeatherProfileMax(:,4),WeatherProfileAvg(:,1),WeatherProfileAvg(:,4))