## General information
Original data were obtained from the [Bundesinstitut für Bau-, Stadt- und Raumforschung (BBSR)](http://www.bbsr-energieeinsparung.de/EnEVPortal/DE/Regelungen/Testreferenzjahre/TRY_node.html), webpage accessed in 09/2016

## Description of profiles
- Original data are in [TRY2011_Datensatz1_2](TRY2011_Datensatz1_2)
- Several profiles we [extracted from Region 7](ExtractedProfiles):
  - the maximum radiation day in the avarage year of 2010
  - persistency predictions for the maximum radiation day
  - test predictions introducing several artificial errors in the data of the maximum radiation day, which has been described in more detailed in master thesis

## Contact
Jan Dinkelbach
